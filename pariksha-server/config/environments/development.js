module.exports = {
  host: '127.0.0.1',
  port: 3005,
  mongoUrl: 'mongodb://localhost:27017/pariksha',
  logLevel: 'debug',
  secret: 'Pariksha',
  // Secret token to generate auth token for authentication
  TOKEN_SECRET: process.env.TOKEN_SECRET ||
      'pvpnCCZfwOF85pBjbOebZiYIDhZ3w9LZrKwBZ7152K89mPCOHtbRlmr5Z91ci4L',
};

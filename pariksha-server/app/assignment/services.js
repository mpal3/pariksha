const mongoose = require( "mongoose" );

const TestDetails = mongoose.model( "TestDetails" );
const QuestionMaster = mongoose.model( "QuestionMaster" );

const getQuestionList = ( ) => {
    return QuestionMaster.find();
};

const submitTest = ( data ) => {
    const testDetails = new TestDetails( data );
    return testDetails.save();
};


module.exports = {
    getQuestionList,
    submitTest,
};

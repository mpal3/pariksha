require( "../../models/question_model" );
require( "../../models/candidate_model" );

const controller = require( "./controller" );
const express = require( "express" );
const router = express.Router( );

router.get( "/secure/getQuestionList", controller.getQuestionList );
router.post( "/secure/submitTest", controller.submitTest );

module.exports = router;

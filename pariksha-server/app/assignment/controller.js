const services = require( "./services" );

exports.getQuestionList = async ( req, res ) => {
    try {
        const getResult = await services.getQuestionList( req.body );
        let testQuestion = generateQuestionlist(getResult);
        res.success( testQuestion );
    } catch ( err ) {
        res.send( err );
    }
};

exports.submitTest = async ( req, res ) => {
    try {
        const getResult = await services.submitTest( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

function generateQuestionlist(allQuestion) {
    let testQuestion = {
        candidateId: 1234,
        questionList: allQuestion
    }
    return testQuestion;
}
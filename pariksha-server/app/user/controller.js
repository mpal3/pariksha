const services = require('./services');
const jwt = require('jsonwebtoken');
const config = require('../../config');


exports.getUserList = async (req, res) => {
  try {
    const getResult = await services.getUserList();
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.addUser = async (req, res) => {
  try {
    const getResult = await services.addUser(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.updateUser = async (req, res) => {
  try {
    const getResult = await services.updateUser(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.getUserRole = async (req, res) => {
  try {
    const getResult = await services.getUserRole(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

/**
 * This Api will authenticate user against its email and password.
 * If authentication is successfull, it returns a authentication token as a
 * response.
 */
exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await services.getUserByEmail(email);

  if (user && user.password == password) {
    // User is authenticated successfully
    const token = jwt.sign({email, role: user.role}, config.TOKEN_SECRET, {
      expiresIn: 86400  // expires in 24 hours
    });
    res.success({
      firstName: user.firstName,
      lastName: user.lastName,
      email: email,
      token
    });
  } else {
    //Authentication failed
    res.send({msg: 'AUTH FAILED'})
  }
}

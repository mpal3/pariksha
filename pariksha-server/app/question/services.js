
const mongoose = require( "mongoose" );

const QuestionMaster = mongoose.model( "QuestionMaster" );
const TechnologyMaster = mongoose.model( "TechnologyMaster" );
const QuestionComplexityMaster = mongoose.model( "QuestionComplexityMaster" );

const getQuestionList = ( ) => {
    return QuestionMaster.find().limit(30);
};

const addQuestion = ( data ) => {
    const questionMaster = new QuestionMaster( data );
    return questionMaster.save();
};

const getTechnologyList = ( ) => {
    return TechnologyMaster.find();
};
const getQuestionComplexity = ( ) => {
    return QuestionComplexityMaster.find();
};


module.exports = {
    getQuestionList,
    addQuestion,
    getTechnologyList,
    getQuestionComplexity
};

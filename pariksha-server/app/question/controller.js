const services = require( "./services" );

exports.getQuestionList = async ( req, res ) => {
    try {
        const getResult = await services.getQuestionList( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

exports.addQuestion = async ( req, res ) => {
    try {
        const getResult = await services.addQuestion( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

exports.getTechnologyList = async ( req, res ) => {
    try {
        const getResult = await services.getTechnologyList( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

exports.getQuestionComplexity = async ( req, res ) => {
    try {
        const getResult = await services.getQuestionComplexity( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};
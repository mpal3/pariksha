require( "../../models/question_model" );
const controller = require( "./controller" );
const express = require( "express" );
const router = express.Router( );

router.get( "/secure/getTechnologyList", controller.getTechnologyList );
router.get( "/secure/getQuestionComplexity", controller.getQuestionComplexity );
router.get( "/secure/getQuestionList", controller.getQuestionList );
router.post( "/secure/addQuestion", controller.addQuestion );
router.post( "/secure/getQuestionList", controller.getQuestionList );

module.exports = router;

const services = require( "./services" );

exports.getCandidateList = async (req, res) => {
    try {
        const candidateList = await services.getCandidatesList();
        res.success(candidateList);
    } catch(err) {
        res.send(err);
    }
};

exports.createAssignment = async ( req, res ) => {
    try {
        const getResult = await services.createAssignment( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};
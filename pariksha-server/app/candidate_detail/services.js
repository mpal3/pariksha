const mongoose = require( "mongoose" );

const CandidateDetail = mongoose.model( "CandidateDetail" );

const getCandidatesList = () => {
    return CandidateDetail.find();
};

const createAssignment = ( data ) => {
    const candidateDetail = new CandidateDetail( data );
    return candidateDetail.save();
};

module.exports = {
    getCandidatesList,
    createAssignment
};

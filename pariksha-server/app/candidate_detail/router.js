require( "../../models/candidate_model" );
const controller = require( "./controller" );
const express = require( "express" );
const router = express.Router( );

router.get( "/secure/getCandidateList", controller.getCandidateList );
router.post( "/secure/createAssignment", controller.createAssignment );

module.exports = router;

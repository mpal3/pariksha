const mongoose = require( "mongoose" );
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const testDetailsSchema = new Schema({
    candidateId: { type: Number, required: true },
    totalMarks: { type: Number, required: true },
    answerDetails: [
        {
            questionId: { type: Number, required: true },
            answer: { type: String },
            marks: { type: Number, required: true },
        }
    ],
}, { 
    collection: 'TestDetails',
    timestamps: true,
} );

module.exports = mongoose.model( "TestDetails", testDetailsSchema );

const candidateDetailSchema = new Schema( {
    candidateId: { type: Number, unique: true, index: true },
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    mobileNo: { type: String, required: true },
    validTill: { type: String, required: true },
    profile: { type: String, required: true },
    isValid: { type: Boolean, required: true },
    score: { type: String, required: true },
    technologies: {
        primaryskill: [],
        secondaryskill: [],
    },
}, { 
    collection: 'CandidateDetail',
    timestamps: true,
} );

candidateDetailSchema.plugin(AutoIncrement, { inc_field: 'candidateId' });

module.exports = mongoose.model( "CandidateDetail", candidateDetailSchema );

const mongoose = require( "mongoose" );
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const questionMasterSchema = new Schema( {
    questionId: { type: Number, unique: true, index: true },
    technologyId: { type: Number, required: true },
    answer: { type: String, required: true },
    question: { type: String, required: true },
    complexity: { type: Number, required: true },
    answerOption: { type: Schema.Types.Mixed, required: true },
}, { 
    collection: 'QuestionMaster',
    timestamps: true,
} );

questionMasterSchema.plugin(AutoIncrement, { inc_field: 'questionId' });

module.exports = mongoose.model( "QuestionMaster", questionMasterSchema );

const questionComplexityMasterSchema = new Schema( {
    complexity: { type: Number, required: true },
    marks: { type: Number, required: true },
}, {
    collection: 'QuestionComplexityMaster',
} );

module.exports = mongoose.model( "QuestionComplexityMaster", questionComplexityMasterSchema );

const technologyMasterSchema = new Schema({
    technologyId: { type: Number, required: true },
    technology: { type: String, required: true },
}, { 
    collection: 'TechnologyMaster',
} );

module.exports = mongoose.model( "TechnologyMaster", technologyMasterSchema );

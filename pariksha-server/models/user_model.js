const mongoose = require( "mongoose" );
const AutoIncrement = require('mongoose-sequence')(mongoose);

const Schema = mongoose.Schema;

const userSchema = new Schema( {
    userId: { type: Number, unique: true, index: true },
    firstName: { type: String, required: [true, 'First Name is required'] },
    lastName: { type: String, required: [true, 'Last Name is required'] },
    email: { type: String, required: [true, 'Email is required'] },
    password: { type: String, required: [true, 'Password is required'] },
    roleId: { type: Number, required: [true, 'Role is required'] },
}, { 
    collection: 'UserMaster',
    timestamps: true,
} );
userSchema.plugin(AutoIncrement, { inc_field: 'userId' });

module.exports = mongoose.model( "UserMaster", userSchema );

const userRoleSchema = new Schema({
    roleId: { type: Number, required: true },
    role: { type: String, required: true },
    
}, { 
    collection: 'UserRole',
} );

module.exports = mongoose.model( "UserRole", userRoleSchema );

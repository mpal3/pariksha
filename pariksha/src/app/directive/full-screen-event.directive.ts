import { Directive,HostListener } from '@angular/core';

@Directive({
  selector: '[appFullScreenEvent]'
})
export class FullScreenEventDirective {

  constructor() { }

  @HostListener('click', ['$event'])
    onClick(event: MouseEvent) {
        console.log(event.currentTarget);
        let elem =  document.getElementById('assignment-body'); 
        let methodToBeInvoked;
        if (elem.requestFullscreen) {
          methodToBeInvoked = elem.requestFullscreen();
        } else if (elem.webkitRequestFullscreen) {
          methodToBeInvoked = elem.webkitRequestFullscreen;
        } else if (elem['mozRequestFullScreen']) {
          methodToBeInvoked = elem['mozRequestFullScreen'];
        } else if (elem['msRequestFullscreen']) {
          methodToBeInvoked = elem['msRequestFullscreen'];
        }
    
        if(methodToBeInvoked) {
          methodToBeInvoked.call(elem);
        }
    }

}

import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appDocumentEvents]'
})
export class DocumentEventsDirective {

  constructor() { }
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    // event.preventDefault();
  }
  @HostListener('document:contextmenu', ['$event'])
    oncontextmenu(event) {
      console.log('right button clicked');
      return false;
    }
}

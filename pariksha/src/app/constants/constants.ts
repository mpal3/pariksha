export const HTTP_CONFIG = {
    HOST_URL: 'http://localhost:3005/',
    AUTH: 'secure',
    // HOST_URL: '',
  };

export const LOCAL_STORAGE = {
  TOKEN: 'authToken',
  USER_DETAIL: 'userDetail'
}
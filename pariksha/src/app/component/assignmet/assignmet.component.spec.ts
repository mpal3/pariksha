import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmetComponent } from './assignmet.component';

describe('AssignmetComponent', () => {
  let component: AssignmetComponent;
  let fixture: ComponentFixture<AssignmetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

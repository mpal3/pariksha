import { Component, OnInit } from '@angular/core';

import { QuestionService } from '../../services/question.service';
import { Question } from '../../models/model';

@Component({
  selector: 'app-assignmet',
  templateUrl: './assignmet.component.html',
  styleUrls: ['./assignmet.component.css']
})
export class AssignmetComponent implements OnInit {
  questionList: Array<Question>;
  isTestStart = false;
  answerList = {};
  result: {};
  testInfo: any; 
  
  //timer varriable
  displayHourMinutTimer: any = '00:00:00';
  endTime: number = 1000000;
  timer: any;

  constructor(
    private questionService: QuestionService,
  ) {
    this.getQuestionList();
   }
  

  ngOnInit() {
  }

  getQuestionList() {
    this.questionService.getQuestionList().subscribe((response: any) => {
      this.testInfo = response.payload;
      this.questionList = response.payload.questionList;
    });
  }

  assignmentTabChanged(evaent) {
    this.submitAssignment()
  }

  submitAssignment() {
    clearTimeout(this.timer);

    let totalMarks = 0;
    let testSummary = [];
    this.questionList.forEach((ques, index) => {
      let ansObj = {
        // questionId: 1, 
        questionId: ques.questionId, 
        answer: '', 
        marks: 0,
        isAttempted: false
      }
      if(this.answerList.hasOwnProperty('ans-' + index)) {
        totalMarks += ques.complexity;
        ansObj.marks = ques.complexity;
        ansObj.answer = ques.answer;
        ansObj.isAttempted = true;
      }
      testSummary.push(ansObj)
    })
    let answerBody = {
      userId: 87654321,
      totalMarks: totalMarks,
      answerList: testSummary,
    
    }

    this.questionService.submitTest(answerBody).subscribe(response => {
      console.log("result: ", response);
      this.result = response.payload;
      this.isTestStart = !this.isTestStart;
      this.answerList = {};
      this.exitFullScreen();
    });
    
    console.log('answerBody: ', answerBody);
    console.log('totalMarks: ', totalMarks);
  }

  fullScreen() {
    this.isTestStart = !this.isTestStart;
    let elem =  document.getElementById('assignment-body');
    let methodToBeInvoked;
    if (elem.requestFullscreen) {
      methodToBeInvoked = elem.requestFullscreen();
    } else if (elem.webkitRequestFullscreen) {
      methodToBeInvoked = elem.webkitRequestFullscreen;
    } else if (elem['mozRequestFullScreen']) {
      methodToBeInvoked = elem['mozRequestFullScreen'];
    } else if (elem['msRequestFullscreen']) {
      methodToBeInvoked = elem['msRequestFullscreen'];
    }
    
    if(methodToBeInvoked) {
      methodToBeInvoked.call(elem);
    }
    this.startTimer();
  }

  exitFullScreen() {
    let elem =  document.getElementById('assignment-body');
    let methodToBeInvoked;
    if (document.exitFullscreen) {
      methodToBeInvoked = document.exitFullscreen();
    }
    else if (document['msExitFullscreen']) {
      methodToBeInvoked = document['msExitFullscreen'];
    }
    else if (document['mozCancelFullScreen']) {
      methodToBeInvoked = document['mozCancelFullScreen'];
    }
    else if (document.webkitExitFullscreen) {
      methodToBeInvoked = document.webkitExitFullscreen();
    }
      
    if(methodToBeInvoked) {
      methodToBeInvoked.call(document);
    }
  }

  startTimer() {
    this.endTime = 1000000;
    this.timer = setInterval(() => this.displayTimer(), 1000);
  }

  displayTimer() {

    this.endTime = this.endTime -1000;
    let dateDifference: any = this.endTime;

    if (dateDifference == 0) {
      // clearTimeout(this.timer);
      // this.isTestStart = !this.isTestStart;
      this.submitAssignment();
    }

    let totalMillisecsLeft = dateDifference;
    let hour = Math.floor(totalMillisecsLeft / (1000 * 60 * 60));
    totalMillisecsLeft = totalMillisecsLeft - hour * (1000 * 60 * 60);
    let minut = Math.floor(totalMillisecsLeft / (1000 * 60 ))
    totalMillisecsLeft = totalMillisecsLeft - minut * (1000 * 60);
    let second = Math.ceil(totalMillisecsLeft / 1000);
    
    this.displayHourMinutTimer = (hour > 9 ? hour : '0' + hour) + ':' +  (minut > 9 ? minut : '0' + minut) + ':' + (second > 9 ? second : '0' + second);
  }

}


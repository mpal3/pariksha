import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import {MatSnackBar} from '@angular/material';

import { QuestionService } from '../../services/question.service';
import { Question } from '../../models/model';
import { SnackbarService } from '../../services/snackbar.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {

  result: Array<Object>;
  technologyList: Array<Object>;
  complexity: Array<Object>;
  questinForm: any;
  techMap: Object = {};

  questionObj: Question = {
    technologyId: 0,
    question: '',
    complexity: 1,
    answer: '',
    answerOption: {
      A: '',
      B: '',
      C: '',
      D: ''
    }
  };

  constructor(
    private questionService: QuestionService,
    private snackbar: SnackbarService,
    private commonservice: CommonService
  ) { 
      this.addValidation();
      this.getTechnologyList();
      this.getComplexity();
   }

  ngOnInit() {
  }

  submit() {
    var self = this;

    this.questionService.addQuestion(this.questionObj).subscribe(response => {
      this.result = response.payload;
      let message = 'You have added ' + this.techMap[this.questionObj.technologyId] + ' question successfully.'
      this.snackbar.openSnackBar(message, 'Dismiss');
      this.setFormPristine();
    });
  }

  getTechnologyList() {
    this.questionService.getTechnologyList().subscribe(response => {
      this.technologyList = response.payload;
      this.createTechMap();

    });
  }

  createTechMap() {
    this.technologyList.forEach((value, key) => {
      this.techMap[value['technologyId']] = value['technology'];
    })
  }

  getComplexity() {
    this.questionService.getComplexity().subscribe(response => {
      this.complexity = response.payload;
    });
  }

  setFormPristine() {
    this.questinForm.reset();
    Object.keys(this.questinForm.controls).forEach(key => {
      this.questinForm.controls[key].setErrors(null)
    });
  }

  getErrorMessage(prop, length) {
    return this.commonservice.getErrorMessage(this.questinForm, prop, length);
  }


  addValidation() {
    this.questinForm = new FormGroup({
      question: new FormControl('', [
          Validators.required
      ]),
      answerGroup: new FormControl('', [
        Validators.required
      ]),
      optionA: new FormControl('', [
        Validators.required
      ]),
      optionB: new FormControl('', [
        Validators.required
      ]),
      optionC: new FormControl('', [
        Validators.required
      ]),
      optionD: new FormControl('', [
        Validators.required
      ]),
      complexity: new FormControl('', [
        Validators.required
      ]),
      technologyId: new FormControl('', [
        Validators.required
      ]),
    });
  }
}

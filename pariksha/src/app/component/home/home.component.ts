import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { LocalStorageService } from '../../services/local-storage.service';
import { LOCAL_STORAGE } from '../../constants/constants';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  opened: boolean = true;
  modeVal: string = 'side';
  currentUrl: string;
  userDetail: Object;

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService
  ) { 
    this.onResize();
  }

  ngOnInit() {
    this.currentUrl = this.router.url;
    console.log("currentUrl: ", this.currentUrl);
    this.userDetail = JSON.parse(this.localStorageService.getData(LOCAL_STORAGE.USER_DETAIL));
  }

  onResize() {
    let width = window.innerWidth;
    if(width < 700) {
      this.modeVal ='over';
      this.opened = false;
    } else {
      this.modeVal ='side'
      this.opened = true;;
    }
  }
  navigateToRoute(link) {
    this.currentUrl = link;
    this.router.navigate([link]);
    if(this.currentUrl == '/login') {
      this.logout();
    }
  }
  
  logout() {
    this.localStorageService.removeSelectedData(LOCAL_STORAGE.TOKEN);
    this.localStorageService.removeSelectedData(LOCAL_STORAGE.USER_DETAIL);
  }

}
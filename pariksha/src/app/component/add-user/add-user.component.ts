import { Component, OnInit, Input, Output, EventEmitter,Inject } from '@angular/core';
import { Validators, FormControl, FormGroup} from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { AddUserService } from '../../services/user.service';
import { UserDetail } from '../../models/model';
import { SnackbarService } from '../../services/snackbar.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  @Output() changeView = new EventEmitter<string>();
  @Input() userDetail: UserDetail;

  addUserForm: any;
  addUserObj: UserDetail = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    roleId: 0
  };
  roleOptions: Array<Object> = []
  categoryOptions: Array<Object> = [];
  result: Array<Object>;
  title: string = 'Add User';


  constructor(
    private userService: AddUserService,
    private snackbar: SnackbarService,
    private commonservice: CommonService,
    public dialogRef: MatDialogRef<AddUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { 
    this.addValidation();
    this.getUserRole();
  }

  ngOnInit() {
    console.log("data",this.data)
    if(Object.keys(this.data).length) {
      this.addUserObj = this.data;
      this.title = 'Edit User';
    }
  }

  navigateToUserList() {
    this.changeView.emit();
  }

  getUserRole() {
    this.userService.getUserRole().subscribe((response: any) => {
      this.roleOptions = response.payload;
    });
  }

  submit() {
    this.userService.addUser(this.addUserObj).subscribe(response => {
      let message;
      if(response.success) {
        this.result = response.payload;
        message = this.addUserObj.userId ? 'you have update user successfully' : 'You have added user successfully.';
        this.setFormPristine();
        this.onDialogClose(this.result);
      } else {
        message = 'Unable to add User.'
      }
      this.snackbar.openSnackBar(message, 'Dismiss');
      
    });
  }

  setFormPristine() {
    this.addUserObj = {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      roleId: 0
    };
    this.addUserForm.reset();
    Object.keys(this.addUserForm.controls).forEach(key => {
      this.addUserForm.controls[key].setErrors(null)
    });    
  }

  getErrorMessage(prop, length) {
    return this.commonservice.getErrorMessage(this.addUserForm, prop, length);
  }
  
  addValidation() {
    this.addUserForm = new FormGroup({
      firstName: new FormControl('', [
          Validators.required
      ]),
      lastName: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) 
      ]),
      password: new FormControl('', [
        Validators.required
      ]),
      roleId: new FormControl('', [
        Validators.required
      ])
    });
  }

  onDialogClose(result): void {
    this.dialogRef.close(result);
  }
}

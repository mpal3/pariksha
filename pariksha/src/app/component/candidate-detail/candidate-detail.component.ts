import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material';

import { CandidateDetailService } from '../../services/candidatedetail.service';

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.css']
})
export class CandidateDetailComponent implements OnInit {

  displayedColumns = ['firstName', 'lastName', 'email', 'mobileNo', 'profile', 'technologies', 'score', 'action'];
  candidateList: any;

  constructor(private candidateDetailService: CandidateDetailService) { 
    this.getCandidateList();
  }

  ngOnInit() {
  }

  getCandidateList() {
    this.candidateDetailService.getCandidateList().subscribe((response: any) => {
      this.candidateList = new MatTableDataSource(response.payload);
    });
  }

}
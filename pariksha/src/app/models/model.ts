export declare interface Logindetail {
  email: string;
  password: string;
}

export declare interface Question {
  questionId?: string,
  answer: string,
  answerOption: {
    A: string,
    B: string,
    C: string,
    D: string
  },
  technologyId: number,
  question: string,
  complexity: number
}

export declare interface UserDetail {
  userId?: number;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  roleId: number;
}

export declare interface CandidateDetail {
  candidateId?: number;
  firstName: string;
  lastName: string;
  email: string;
  mobileNo: string;
  technologies: {
    primaryskill: string[],
    secondaryskill: string[],
  };
  validTill: string;
  profile: string;
}
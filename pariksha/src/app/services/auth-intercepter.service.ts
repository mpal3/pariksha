import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpUserEvent, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs";
// import { UserService } from "../shared/user.service";
// import 'rxjs/add/operator/do';
import { Router } from "@angular/router";

import { LocalStorageService } from '../services/local-storage.service';
import { LOCAL_STORAGE } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class AuthIntercepterService implements HttpInterceptor {

  constructor(
      private router: Router, 
      private localStorageService: LocalStorageService
    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token = this.localStorageService.getData(LOCAL_STORAGE.TOKEN);
        
        if (req.url.indexOf('secure') == -1){
            return next.handle(req.clone());
        }

        if (token != null) {
            const clonedreq = req.clone({
                headers: req.headers.set("x-access-token", token )
            });
            return next.handle(clonedreq);
        } else {
            this.router.navigateByUrl('/login');
        }
    }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HTTP_CONFIG } from '../constants/constants';
import { CandidateDetail } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class CandidateDetailService {
  private baseUrl = HTTP_CONFIG.HOST_URL;
  private secure = HTTP_CONFIG.AUTH;

  constructor(private http: HttpClient) { }

  getCandidateList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/getCandidateList');
  }

  createAssignment(assignmentDetail: CandidateDetail): Observable<ServerResponse> {
    return this.http.post<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure + '/createAssignment', assignmentDetail
    )
  }
}

export declare interface ServerResponse {
  success: boolean;
  payload: Array<Object>
}
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  getErrorMessage(form, prop, length) {
    let errMessage = 'Required';
    if(form.controls[prop].hasError('required')) {
      errMessage = 'Required';
    } else if (form.controls[prop].hasError('pattern')) {
      errMessage = 'Not a valid ' + prop;
    } else if (form.controls[prop].hasError('minlength')) {
      errMessage = 'Required length is at least ' + length + ' characters';
    }
    return errMessage;
  }
}

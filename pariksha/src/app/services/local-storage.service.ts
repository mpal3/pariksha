import { Injectable } from '@angular/core';

import { LOCAL_STORAGE } from '../constants/constants';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }
  setData(key: string, value: any ) {
    localStorage.setItem(key, value);
  }

  getData(key: string) {
    return localStorage.getItem(key);
  }

  removeSelectedData(key: string) {
    localStorage.removeItem(key);
  }

  removeAllData() {
    localStorage.clear();
  }
}